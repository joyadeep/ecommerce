import Login from "./pages/Login";
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Homepage from "./pages/Homepage";

const App=()=> {
  return (
    <>
   <BrowserRouter>
    <Routes>
    <Route path="/" element={<Homepage/>}/>
      <Route path="/login" element={<Login/>}/>
    </Routes>
   </BrowserRouter>
    </>
  );
}

export default App;
