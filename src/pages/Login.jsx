import React from 'react'
import {Box, Button, Paper, TextField, Typography,Stack} from '@mui/material'
import {useFormik} from 'formik'
import loginSchema from '../schema/LoginSchema'
import {Link} from 'react-router-dom'


const initialValues={
    username:'',
    password:''
}

const Login = () => {
    const {values,handleSubmit,errors,handleChange,handleBlur}=useFormik(
        {
            initialValues:initialValues,
            validationSchema:loginSchema,
            onSubmit(values){
                console.log("Values:",values);
            }
        }
    )
  return (
   <Box sx={{widht:'100%',height:'100vh',bgcolor:'white',display:'flex',justifyContent:'center',alignItems:'center',bgcolor:'#F5F5F5'}}>
        <Paper elevation={1} sx={{p:2,width:{xs:'100%',sm:'100%',md:'50%',lg:'35%'}}}>
            <Typography variant="h3" align='center' fontWeight={'500'} letterSpacing="1px" color="#303030" mb={4} >Login</Typography>
            <form onSubmit={handleSubmit}>
            <Stack spacing={2}>
              <Box>
           <TextField variant='outlined' error={Boolean(errors.username)} type="text" name="username" value={values.username} onChange={handleChange} onBlur={handleBlur} label="Email or Username" fullWidth/>
           <Typography sx={{fontSize:'12px', color:'red',height:'15px'}}>{errors.username}</Typography>
              </Box>
            <Box>
            <TextField variant='outlined' error={Boolean(errors.password)} type="password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} label="Password" fullWidth/>
            <Typography sx={{fontSize:'12px', color:'red',height:'15px'}}>{errors.password}</Typography>
              </Box>
            <Button variant="contained" type="submit" disableElevation size="small" sx={{textTransform:'none',fontWeight:'500',fontSize:'20px'}}>Login</Button>
            </Stack>
                
            </form>

            <Typography mt={2} sx={{fontSize:'16px'}}>New to Ecommerce? <Link to="/" style={{textDecoration:'none'}} >SignUp</Link> </Typography>
        </Paper>
   </Box>
  )
}

export default Login