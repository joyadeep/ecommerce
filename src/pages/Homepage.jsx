import { AppBar, Box, Button, Toolbar, Typography } from '@mui/material'
import React from 'react'
import {Link} from 'react-router-dom'

const Homepage = () => {
  return (
    <>
   <AppBar elevation={0} position="static">
    <Toolbar>
        <Typography flexGrow={1}>Ecommerce</Typography>
       <Typography>
        <Link to="/login" style={{color:'inherit',textDecoration:'none'}}>Login</Link>
       </Typography>
       <Typography sx={{ml:2}}>
       <Link to="/" style={{color:'inherit',textDecoration:'none'}}>Signup</Link>
       </Typography>
    </Toolbar>
   </AppBar>
   <Box>
    <Typography variant="h4">
        Landing Page 
    </Typography>
   </Box>
   </>
  )
}

export default Homepage